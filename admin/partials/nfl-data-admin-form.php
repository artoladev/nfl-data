<?php
/**
 * Provide an admin area view of the plugin settings form.
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 * @package    NFL_Data
 * @subpackage NFL_Data/admin/partials
 */

?>
<div class="nfl-data-admin-wrapper">
	<form method="post" action="options.php">
		<h1> <?php echo esc_html( get_admin_page_title() ); ?> </h1>
		<div class="section--settings">
			<h2>API Settings</h2>
			<?php do_settings_fields( 'nfl-data', 'nfl-data-settings-section' ); ?>
		</div>
		<div class="section--settings">
			<h2>Content Settings</h2>
			<?php do_settings_fields( 'nfl-data', 'nfl-data-labels-section' ); ?>
		</div>
		<?php
			settings_fields( 'nfl-data' );
			submit_button();
		?>
	</form>
</div>
