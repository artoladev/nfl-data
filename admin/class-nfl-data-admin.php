<?php
/**
 * The file that defines the admin data class
 *
 * This class define how the NFL-Data creates the necessary
 * functionality to define the admin area of the plugin.
 *
 * @since      1.0.0
 * @package    NFL_Data
 * @subpackage NFL_Data/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the the menu options, labels and settings
 * fields present in the admin area.
 *
 * @package    Nfl_Data
 * @subpackage Nfl_Data/admin
 * @author     Adan Artola <adanartola@gmail.com>
 */
class Nfl_Data_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $nfl_data    The ID of this plugin.
	 */
	private $nfl_data;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since      1.0.0
	 * @param      string $nfl_data   The name of this plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $nfl_data, $version ) {

		$this->nfl_data = $nfl_data;
		$this->version  = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * Loading the css bundle for the admin area.
		 * To see the source code definition for this bundle, review
		 * resources/src/scss-admin.
		 */
		wp_enqueue_style( $this->nfl_data, plugin_dir_url( __DIR__ ) . 'assets/css/admin-bundle.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * Loading the javascript bundle for the admin area.
		 * To see the source code definition for this bundle, review
		 * resources/src/wpjs.
		 */
		wp_enqueue_script( $this->nfl_data, plugin_dir_url( __DIR__ ) . 'assets/wpjs/bundle.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Register the Admin settings form fields of the NFL-Data plugin.
	 *
	 * @since    1.0.0
	 */
	public function register_admin_fields() {

		/**
		 * Define settings options for the API service.
		 */

		/**
		 * Define settings section.
		 */
		add_settings_section(
			'nfl-data-settings-section',
			'',
			null,
			'nfl-data'
		);

		/**
		 * Registering into WordPress API URL field.
		 *
		 * This field is going to define the base URL
		 * for the API Service.
		 */
		register_setting(
			'nfl-data',
			'nfl-data-api-url',
			array(
				'type'         => 'string',
				'show_in_rest' => false,
			)
		);

		/**
		 * Adding API URL field, name, type, description, and placeholder
		 * needed for rendering purposes.
		 */
		add_settings_field(
			'nfl-data-api-url',
			__( 'API Service URL', 'nfl-data' ),
			function() {
				$this->build_text_input(
					'nfl-data-api-url',
					'text',
					'e.g. http://delivery.chalk247.com/team_list/NFL.JSON',
					__( 'Add the API Service URL', 'nfl-data' )
				);
			},
			'nfl-data',
			'nfl-data-settings-section',
			array( 'label_for' => 'nfl-data-api-url' )
		);

		/**
		 * Registering into WordPress the API Key field.
		 *
		 * This field is going to be the api_key attribute
		 * necessary to retrieve information from the API.       *
		 */
		register_setting(
			'nfl-data',
			'nfl-data-api-key',
			array(
				'type'         => 'string',
				'show_in_rest' => false,
			)
		);

		/**
		 * Adding API Key, name, type, description, and placeholder
		 * needed for rendering purposes.
		 */
		add_settings_field(
			'nfl-data-api-key',
			__( 'API Service Key', 'nfl-data' ),
			function() {
				$this->build_text_input(
					'nfl-data-api-key',
					'text',
					'e.g. 74db8efa2a6db279393b433d97c2bc843f8e32b0',
					__( 'Add the API Service \'API Key\' ', 'nfl-data' )
				);
			},
			'nfl-data',
			'nfl-data-settings-section',
			array( 'label_for' => 'nfl-data-api-key' )
		);

		/**
		 * Define the content labels section.
		 */
		add_settings_section(
			'nfl-data-labels-section',
			'',
			null,
			'nfl-data'
		);

		/**
		 * Registering into WordPress the The teams list header field.
		 *
		 * This field defines the text present in the header title
		 * of the teams list.
		 */
		register_setting(
			'nfl-data',
			'nfl-data-table-header',
			array(
				'type'         => 'string',
				'show_in_rest' => false,
			)
		);

		/**
		 * Adding Team list header, name, type, description, and placeholder
		 * needed for rendering purposes.
		 */
		add_settings_field(
			'nfl-data-table-header',
			__( 'Teams List Header', 'nfl-data' ),
			function() {
				$this->build_text_input(
					'nfl-data-table-header',
					'text',
					'e.g. NFL Teams List',
					__( 'Add the NLF Teams List header', 'nfl-data' )
				);
			},
			'nfl-data',
			'nfl-data-labels-section',
			array( 'label_for' => 'nfl-data-table-header' )
		);

		/**
		 * Registering into WordPress The teams list subheader field.
		 *
		 * This field defines the text present in the subheader title
		 * of the teams list.
		 */
		register_setting(
			'nfl-data',
			'nfl-data-table-subheader',
			array(
				'type'         => 'string',
				'show_in_rest' => false,
			)
		);

		/**
		 * Adding Team list subheader, name, type, description, and placeholder
		 * needed for rendering purposes.
		 */
		add_settings_field(
			'nfl-data-table-subheader',
			__( 'Teams List Subheader', 'nfl-data' ),
			function() {
				$this->build_text_input(
					'nfl-data-table-subheader',
					'text',
					'e.g. NFL Teams List',
					__( 'Add the NLF Teams List subheader', 'nfl-data' )
				);
			},
			'nfl-data',
			'nfl-data-labels-section',
			array( 'label_for' => 'nfl-data-table-subheader' )
		);

	}

	/**
	 * Register  NFL Data Admin menupage.
	 *
	 * Defines the menu name, page title and
	 * actions to be present in the WordPress
	 * admin menu.
	 *
	 * @since    1.0.0
	 */
	public function register_admin_menu() {

		add_menu_page(
			__( 'NFL Data Settings', 'nfl-data' ),
			'NFL Data',
			'manage_options',
			'nfl-data',
			function() {
				$this->plugin_admin_page();}
		);

	}

	/**
	 * Defines the actions  for the  NFL Data admin pages.
	 *
	 * @since    1.0.0
	 */
	public function plugin_admin_page() {

		if ( current_user_can( 'manage_options' ) ) {
			/**
			 * Render the settings folder if the user has the necessary permissions.
			 */
			include plugin_dir_path( __FILE__ ) . 'partials/nfl-data-admin-form.php';
		} else {
			/**
			 * Stops WordPress execution if the user does not have the necessary permisions.
			 */
			wp_die( esc_html__( 'You don\'t have sufficient permissions to access this page.', 'nfl-data' ) );

		}

	}

	/**
	 * Builds the input fields structure for the setting inputs in the NFL Data admin page form.
	 *
	 * @since    1.0.0
	 * @param    string $setting_name  Name attribute of the setting field to render.
	 * @param    string $type          Type attribute of the setting field to render.
	 * @param    string $placeholder   Placeholder attribute of the setting field to render.
	 * @param    string $description   Description of the setting field to render.
	 */
	public function build_text_input( $setting_name, $type, $placeholder = false, $description = false ) {

		$option_value = get_option( $setting_name, '' );
		printf(
			'<input type="%s" id="%s" name="%s" value="%s" style="width: 100%%" autocomplete="off" placeholder="%s" />',
			esc_attr( $type ),
			esc_attr( $setting_name ),
			esc_attr( $setting_name ),
			esc_attr( $option_value ),
			esc_attr( $placeholder )
		);
		if ( $description ) {
			printf(
				'<p class="description">%s</p>',
				esc_attr( $description )
			);
		}

	}

}
