/**
 * Imports all the necessary
 * JS functionalit for the NFL Data plugin.
 *
 * @since      1.0.0
 *
 * @package    nfl_data
 * @subpackage nfl_data/assets/js
 */

/**
 * Import javascript components
 */
import table from './components/table.js';

/**
 * Event listener to load JS functionality on load.
 *  
 * @since      1.0.0
 * @package    nfl_data
 * @subpackage nfl_data/assets/js
 * @author     Adan Artola <adanartola@gmail.com>
 */
window.addEventListener(
	'load',
	() => {
		table();
	}
);
