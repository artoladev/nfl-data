/**
 * NFL data tables javascript functionality.
 *
 * @since      1.0.0
 *
 * @package    nfl_data
 * @subpackage nfl_data/components
 */

/**
 * Import JS frameworks
 */
import $ from 'jquery'; //loading Jquery library.
import 'datatables.net'; //Loading datatables library.

/**
 * Init datatables functionality on NFL tables.
 *  
 * @since      1.0.0
 * @package    nfl_data
 * @subpackage nfl_data/assets/js
 * @author     Adan Artola <adanartola@gmail.com>
 */
export default function initTables()
{    
    
    /**
     * Initialize all the data tables with class .nfl-teams-list
     */
    $('.nfl-teams-list').DataTable({
        /**
         * Adding responsive funtionality for datatables.
         */
        "responsive": true,
        /**
         * Translating table labels with values
         * added from the class-nfl-data-teams file.
         */
        "oLanguage": {
                "sSearch": NFL_DATA_JS.teams.search, //Search field label.
                "sLengthMenu": NFL_DATA_JS.teams.show, //Show number of teams label.
                "sInfo": NFL_DATA_JS.teams.info, //Search field label.
                "oPaginate": {
                    "sPrevious": NFL_DATA_JS.teams.prev, //Previous pagination label.
                    "sNext": NFL_DATA_JS.teams.next //Next pagination label.
                }                
          }
    });

}