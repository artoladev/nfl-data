/**
 * Gulp configuration file
 *
 * @since             1.0.0
 * @package           NFL_DATA
 *
 * Plugin Name:       NFL Data
 * Plugin URI:
 * Description:       Front end assets manager.
 * Version:           1.0.0
 * Author:            Adan Artola
 * Author URI:        https://www.linkedin.com/in/adanartola/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

import del from 'del';
import imagemin from 'gulp-imagemin';
import { src, dest, watch, series, parallel } from 'gulp';
import yargs from 'yargs';
import sass from 'gulp-sass';
import cleanCss from 'gulp-clean-css';
import gulpif from 'gulp-if';
import postcss from 'gulp-postcss';
import sourcemaps from 'gulp-sourcemaps';
import autoprefixer from 'autoprefixer';
import webpack from 'webpack-stream';
import named from 'vinyl-named';
import DependencyExtractionWebpackPlugin from '@wordpress/dependency-extraction-webpack-plugin';
import browserSync from "browser-sync";

const theme_path = '../assets'
const PRODUCTION = yargs.argv.prod;

export const styles = () => {
	return src( 'src/scss/bundle.scss' )
	  .pipe( gulpif( ! PRODUCTION, sourcemaps.init() ) )
	  .pipe( sass().on( 'error', sass.logError ) )
	  .pipe( gulpif( PRODUCTION, postcss( [ autoprefixer ] ) ) )
	  .pipe( gulpif( PRODUCTION, cleanCss( {compatibility:'ie8'} ) ) )
	  .pipe( gulpif( ! PRODUCTION, sourcemaps.write() ) )
	  .pipe( dest( theme_path + '/css' ) );
  }

export const admin_styles = () => {
	return src( ['src/scss-admin/admin-bundle.scss'] )
	  .pipe( gulpif( ! PRODUCTION, sourcemaps.init() ) )
	  .pipe( sass().on( 'error', sass.logError ) )
	  .pipe( gulpif( PRODUCTION, postcss( [ autoprefixer ] ) ) )
	  .pipe( gulpif( PRODUCTION, cleanCss( {compatibility:'ie8'} ) ) )
	  .pipe( gulpif( ! PRODUCTION, sourcemaps.write() ) )
	  .pipe( dest( theme_path + '/css' ) );
  }

export const images = () => {
	return src( 'src/images/**/*.{jpg,jpeg,png,svg,gif}' )
	  .pipe( gulpif( PRODUCTION, imagemin() ) )
	  .pipe( dest( theme_path + '/images' ) );
  }

export const fonts = () => {
	return src( 'src/fonts/**/*.{eot,svg,gif,ttf,woff}' )
	  .pipe( dest( theme_path + '/fonts' ) );
  }

export const copy = () => {
	return src( ['src/**/*','!src/{images,js,wpjs,scss,fonts,scss-admin}','!src/{images,js,wpjs,scss,fonts,scss-admin}/**/*'] )
	  .pipe( dest( theme_path ) );
  }

export const clean = () => {
	return del( [theme_path], {force: true} );
  }

export const scripts = () => {
	return src( ['src/js/bundle.js'] )
	.pipe( named() )
	.pipe(
		webpack(
			{
				module: {
					rules: [
						{
							test: /\.js$/,
							use: {
								loader: 'babel-loader',
								options: {
									presets: ['@babel/preset-env']
								}
							}							
						},
					],
				},
				mode: PRODUCTION ? 'production' : 'development',
				devtool: ! PRODUCTION ? 'inline-source-map' : false,
				output: {
					filename: '[name].js'
				},
				externals: {
					jquery: 'jQuery'
				},
			}
		)
	)
	.pipe( dest( theme_path + '/js' ) );
  }

  export const wpscripts = () => {
		return src( ['src/wpjs/bundle.js'] )
		.pipe( named() )
		.pipe(
			webpack(
				{
					module: {
						rules: [
						{
							test: /\.js$/,
							exclude: /node_modules/,
							use: {
								loader: 'babel-loader',
								options: {
									presets: ['@wordpress/babel-preset-default'],
								}
							}
						}
						]
					},
					mode: PRODUCTION ? 'production' : 'development',
					devtool: ! PRODUCTION ? 'inline-source-map' : false,
					output: {
						filename: '[name].js'
					},
				}
			)
		)
	.pipe( dest( theme_path + '/wpjs' ) );
	}

	export const watchForChanges = () => {
		watch( 'src/scss/**/*.scss', styles );
		watch( 'src/js/**/*.js', scripts );
		watch( 'src/wpjs/**/*.js', wpscripts );
		watch( 'src/scss-admin/**/*.scss', admin_styles );
		watch( 'src/images/**/*.{jpg,jpeg,png,svg,gif}', images );
		watch( 'src/fonts/**/*.{eot,svg,gif,ttf,woff}', fonts );
		watch( ['src/**/*','!src/{images,js,scss}','!src/{images,js,scss}/**/*'], copy );
	}

	export const dev = series( clean, parallel( styles, admin_styles, images, copy, scripts, wpscripts,fonts ), watchForChanges );
	export const build = series( clean, parallel( styles, admin_styles, images, copy, scripts, wpscripts, fonts ) );
	export default dev;
