# NFL Data plugin

Adds a custom shortcode to WordPress which upon usage publishes a table with a list of NFL Teams.

## Installation
- [ ] Download the project folder in a zip file 
- [ ] Unzip the folder and create a new zip file named 'nfl-data' with only the source code files
- [ ] Go to your WordPress Admin -> Plugins -> Add new
- [ ] Click on 'Upload plugin'
- [ ] Choose the plugin's zip file location
- [ ] Click on install now
- [ ] Install and activate the plugin
- [ ] Go to WP Admin -> NFL Data 
- [ ] Add the API's base URL and key to connect it
- [ ] (Optional) Add your Header and subheader
- [ ] Add the [nflteamlist] shortcode to your WordPress page to load the NFL Team List Information.
- [ ] Open your WordPress page to see the loaded list.

## Resources Folder
This folder is in the project for the purposes of the challenge. The resources folder contains the source code for the necessary JS, SCSS, and images assets of the plugin. To build the assets use the following instructions:

## Resources Setup
- [ ] Open a terminal inside the resources folder
- [ ] Run the following commands:

```
nvm install
```
```
nvm use
```
```
npm install
```
```
npm start
```
## 

## Autor: 
Adan Artola 

