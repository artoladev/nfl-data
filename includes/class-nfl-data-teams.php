<?php
/**
 * File containing the class for teams data
 * management.
 *
 * @since      1.0.0
 *
 * @package    nfl_data
 * @subpackage nfl_datta/includes
 */

/**
 * Teams data
 *
 * Defines the methods used to retrieve the teams data from the API,
 * handles the public facing labels of the team data, and adds
 * access to the API status.
 *
 * @package    nfl_data
 * @subpackage nfl_data/includes
 * @author     Adan Artola <adanartola@gmail.com>
 */
class NFL_Data_Teams {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $nfl_data    The ID of this plugin.
	 */
	private $nfl_data;

	/**
	 * The team list.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $teams_list Current list of NFL Teams
	 */
	private $teams_list;

	/**
	 * The columns.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $teams_list_columns Columns in the team list
	 */
	private $teams_list_columns;


	/**
	 * Status message.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $status_message Message for the current connection status.
	 */
	private $status_message;

	/**
	 * API Service object
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $api_service   API service object
	 */
	private $api_service;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $nfl_data      The name of the plugin.
	 */
	public function __construct( $nfl_data ) {
		$this->nfl_data           = $nfl_data;
		$this->api_service        = new NFL_Data_Api_Service( $this->nfl_data );
		$this->teams_list         = array();
		$this->teams_list_columns = array();
	}

	/**
	 * Get team list
	 *
	 * @since    1.0.0
	 * @return   array  Associative array with the teams list loaded from the API.
	 */
	public function get_teams_list() {
		return $this->teams_list;

	}

	/**
	 * Get the status message
	 *
	 * @since    1.0.0
	 * @return   string Returns the API status messages.
	 */
	public function get_status_message() {
		return $this->status_message;

	}

	/**
	 * Get Teams List header
	 *
	 * @since    1.0.0
	 * @return   string Teams list Header.
	 */
	public function get_teams_list_header() {
		return trim( get_option( 'nfl-data-table-header' ) );
	}

	/**
	 * Get Teams List subheader
	 *
	 * @since    1.0.0
	 * @return   string Teams list subheader.
	 */
	public function get_teams_list_subheader() {
		return trim( get_option( 'nfl-data-table-subheader' ) );
	}

	/**
	 * Get team list.
	 *
	 * @since    1.0.0
	 * @return   array Associative array with the columns names.
	 */
	public function get_list_columns() {
		return $this->teams_list_columns;
	}

	/**
	 * Sets the team_list and teams_list variables
	 * if he API status is active and resturns true. If the API status is
	 * failed sets the status message  and returns false.
	 *
	 * @since    1.0.0
	 * @return   bool  true if the data is loaded and false if don't.
	 */
	public function load_teams_list() {

		/**
		 * Actual API call.
		 */
		$response = $this->api_service->get_teams_list();

		/**
		 * If the returning array is empty
		 * sets the object status message.
		 */
		if ( count( $response ) === 0 ) {
			$this->status_message = $this->api_service->get_status_message();
			return false;
		}

		/**
		 * If the API call was susccesfull sets the teams_list and
		 * teams_list_columns.
		 */
		$this->teams_list         = $response['results']->data->team;
		$this->teams_list_columns = $this->update_colum_labels( (array) $response['results']->columns );

		return true;
	}


	/**
	 * Updates columns labels.
	 *
	 * @since    1.0.0
	 * @param    array $columns Raw teams list columns labels.
	 * @return   array Updated teams list columns labels.
	 */
	public function update_colum_labels( $columns ) {

		$new_columns = array();

		/**
		 * Assuming that the name of the columns
		 * it's not going to change
		 */
		foreach ( $columns as $key => $column_label ) {
			switch ( $column_label ) {
				case 'name':
					$new_columns[ $key ] = __( 'Name', 'nfl-data' );
					break;
				case 'NickName':
					$new_columns[ $key ] = __( 'Nickname', 'nfl-data' );
					break;
				case 'display name':
					$new_columns[ $key ] = __( 'Display Name', 'nfl-data' );
					break;
				case 'id':
					$new_columns[ $key ] = __( 'ID', 'nfl-data' );
					break;
				case 'conference':
					$new_columns[ $key ] = __( 'Conference', 'nfl-data' );
					break;
				case 'division':
					$new_columns[ $key ] = __( 'Division', 'nfl-data' );
					break;
				default:
					$new_columns[ $key ] = $column_label;
					break;
			}
		}

		return $new_columns;

	}

	/**
	 * Teams list table labels.
	 *
	 * @since    1.0.0
	 * @return   array Associative arrary with the teams list table labels.
	 */
	public static function get_table_labels() {

		return array(
			'search' => __( 'Search Team:', 'nfl-data' ),
			'show'   => __( 'Show _MENU_ Teams', 'nfl-data' ),
			'info'   => __( 'Showing _START_ to _END_ of _TOTAL_ Teams', 'nfl-data' ),
			'prev'   => __( 'Prev', 'nfl-data' ),
			'next'   => __( 'Next', 'nfl-data' ),
		);

	}


}
