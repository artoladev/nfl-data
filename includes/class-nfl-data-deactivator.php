<?php
/**
 * Fired during plugin deactivation
 *
 * @since      1.0.0
 *
 * @package    NFL_Data
 * @subpackage NFL_Data/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    NFL_Data
 * @subpackage NFL_Data/includes
 * @author     Adan Artola <adanartola@gmail.com>
 */
class NFL_Data_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
