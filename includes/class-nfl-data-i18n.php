<?php
/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 *
 * @package    NFL_Data
 * @subpackage NFL_Data/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation. The first language translation
 * for this plusgin is in Spanish. You can find the translation file in
 * the language folder.
 *
 * @since      1.0.0
 * @package    NFL_Data
 * @subpackage NFL_Data/includes
 * @author     Adan Artola <adanartola@gmail.com>
 */
class NFL_Data_I18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'nfl-data',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
