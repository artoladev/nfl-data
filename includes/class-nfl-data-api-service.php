<?php
/**
 * File containing the class for the API service.
 *
 * @since      1.0.0
 *
 * @package    nfl_data
 * @subpackage nfl_data/includes
 */

/**
 * API Service connection.
 *
 * Defines methods for loading API connection parameters,
 * retrieves teams information from the API, defines the API connection
 * status, logs the errors found in the connection flow.
 * to the API service.
 *
 * @package    nfl_data
 * @subpackage nfl_datta/includes
 * @author     Adan Artola <adanartola@gmail.com>
 */
class NFL_Data_Api_Service {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $nfl_data    The ID of this plugin.
	 */
	private $nfl_data;

	/**
	 * API Key of the service.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $api_key;


	/**
	 * API URL.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $api_base_url   API service URL
	 */
	private $api_base_url;

	/**
	 * API Status.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $api_status   API connection status.
	 */
	private $api_status;

	/**
	 * Connection status constant.
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      CONST    ACTIVE  Active status for the API connection.
	 */
	const ACTIVE = 'ACTIVE';

	/**
	 * Connection status constant.
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      CONST    INVALID_CREDENTIALS  Invalid credentials for the API.
	 */
	const INVALID_CREDENTIALS = 'INVALID_CREDENTIALS';

	/**
	 * Connection status constant.
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      CONST    FAILED    FAILED status for API connection.
	 */
	const FAILED = 'FAILED';

	/**
	 * Status message.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $status_message Message for the current connection status.
	 */
	private $status_message;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $nfl_data      The name of the plugin.
	 */
	public function __construct( $nfl_data ) {

		$this->nfl_data = $nfl_data;
		$this->load_settings_parameters();

	}

	/**
	 * Gets the API Connection Status.
	 *
	 * @since    1.0.0
	 * @return   string API connection current status.
	 */
	public function get_status() {

		return $this->api_status;

	}

	/**
	 * Get the status message
	 *
	 * @since    1.0.0
	 * @return   string  API current status message.
	 */
	public function get_status_message() {

		return $this->status_message;

	}

	/**
	 * Sets the api_key and api_base_url configuration
	 * parameters from the database or sets API status
	 * to Invalid credentials if one parameter is empty.
	 *
	 * @since    1.0.0
	 */
	private function load_settings_parameters() {

		$this->api_key      = trim( sanitize_text_field( get_option( 'nfl-data-api-key' ) ) );
		$this->api_base_url = trim( sanitize_text_field( get_option( 'nfl-data-api-url' ) ) );

		/**
		 * If at least one of the connection parameters  is empty
		 * the conection status change to invalid credentials.
		 */
		if ( empty( $this->api_key ) || empty( $this->api_base_url ) ) {
			$this->api_status = $this::INVALID_CREDENTIALS;
		}

	}

	/**
	 * Get team list.
	 *
	 * @since    1.0.0
	 * @return   array  API response data.
	 */
	public function get_teams_list() {

		$data = array(
			'api_key' => $this->api_key,
		);

		$query    = http_build_query( $data );
		$endpoint = $this->api_base_url . '?' . $query;

		$args = array(
			'method' => 'GET',
		);

		/**
		 * Assuming that the size of the list it's
		 * always going to be less than 100 Teams
		 * only one call to the API it's necessary
		 * to grab the data.
		 */
		$response = $this->request( $endpoint, $args );

		/**
		 * Returns the array with teams data if
		 * there is a  succesful connection and valid creadentials are
		 * available. Returns an empty array if don't.
		 */
		return $response;

	}

	/**
	 * Fetch API requests through WordPress wp_remote_request function.
	 *
	 * @since    1.0.0
	 * @param    string $endpoint API URL plus parementers in a get call.
	 * @param    string $args Arguments required for wp_remote_request function.
	 *
	 * @return   array  Associative array with the API response or empty array if connection failed.
	 */
	private function request( $endpoint, $args ) {

		/**
		 * Handle exceptions while connecting with API services.
		 */
		try {

			/**
			 * Handles empty credential parameters.
			 */
			if ( $this->api_status !== $this::INVALID_CREDENTIALS ) {

				$response = wp_remote_request( $endpoint, $args );

				/**
				 * Handles connection error
				 */
				if ( ! is_wp_error( $response ) ) {

					$response = json_decode( $response['body'] );

					/**
					 * Handles incorrect credential parameters.
					 */
					if ( ! isset( $response->results->error ) ) {

						$this->api_status = $this::ACTIVE;

						return (array) $response;
					}

					$this->api_status = $this::FAILED;

				} else {

					$this->api_status = $this::FAILED;

				}
			}
		} catch ( Exception $e ) {

			$this->api_status = $this::FAILED;

		}

		$this->handle_status_message();
		return array();

	}

	/**
	 * Handles the status message for the current API status.
	 *
	 * @since    1.0.0
	 * @return   string  API current status message.
	 */
	private function handle_status_message() {

		switch ( $this->api_status ) {
			case 'FAILED':
				$this->status_message = __( 'Error connecting to data services. Please review the admin settings or try again later.', 'nfl-data' );
				break;
			case 'INVALID_CREDENTIALS':
				$this->status_message = __( 'Invalid credentials. Please review the admin settings.', 'nfl-data' );
				break;
			default:
				break;
		}

		$log_message = sprintf( 'nfl-data: %s', $this->status_message );

		/**
		 * Logs error message into WordPress log default method.
		 */
		$this->write_log( $log_message );

		return $this->status_message;

	}

	/**
	 * Writes NFL Data errors into WordPress log
	 *
	 * @since    1.0.0
	 * @param    string $log Error or comment to write into WordPress log.
	 */
	public function write_log( $log ) {

		if ( defined( 'WP_DEBUG' ) && WP_DEBUG === true ) {
			// phpcs:disable WordPress.PHP.DevelopmentFunctions
			if ( is_array( $log ) || is_object( $log ) ) {
				error_log( print_r( $log, true ) );
			} else {
				error_log( print_r( $log, true ) );
			}
			// phpcs:enable
		}

	}

}
