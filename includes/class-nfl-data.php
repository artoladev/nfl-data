<?php
/**
 * The file that defines the core plugin class
 *
 * This class define how the NFL-Data creates the necessary
 * hooks to link its functionality into WordPress.
 *
 * @since      1.0.0
 *
 * @package    NFL_Data
 * @subpackage NFL_Data/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    NFL_Data
 * @subpackage NFL_Data/includes
 * @author     Adan Artola <adanartola@gmail.com>
 */
class NFL_Data {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      NFL_Data_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $NFL_Data    The string used to uniquely identify this plugin.
	 */
	protected $nfl_data;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'NFL_DATA_VERSION' ) ) {
			$this->version = NFL_DATA_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->nfl_data = 'nfl-data';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - NFL_Data_Loader. Orchestrates the hooks of the plugin.
	 * - NFL_Data_i18n. Defines internationalization functionality.
	 * - NFL_Data_Admin. Defines all the functionality for the admin area.
	 * - NFL_Data_Public. Defines all  the functionality for the public side of the site.
	 * - NFL_Data_Api_Service. Defines the connection methods to the API service.
	 * - NFL_Data_Teams.  Controller class to process teams data.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-nfl-data-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-nfl-data-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-nfl-data-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the NFL Data plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-nfl-data-public.php';

		/**
		 * The class responsible to connect with API Service functionality
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-nfl-data-api-service.php';

		/**
		 * Controller funtions of teams data
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-nfl-data-teams.php';

		$this->loader = new NFL_Data_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the NFL_Data_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new NFL_Data_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new NFL_Data_Admin( $this->get_nfl_data(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_admin_fields' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'register_admin_menu' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new NFL_Data_Public( $this->get_nfl_data(), $this->get_version() );

		/**
		 * Add the custom JS and CSS created fro this plugin.
		 */
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		/**
		 * Initialize NFL-data shortcode
		 */
		$this->loader->add_action( 'init', $plugin_public, 'add_team_list_shortcode', 10 );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_nfl_data() {
		return $this->nfl_data;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    NFL_Data_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
