��          �      �           	          )     H     `      ~  
   �     �     �  W   �       6        R     d     i     n     w     |     �  )   �     �     �  �  �     �     �      �     �  1   �  4   /     d     p  	   �  g   �     �  Z   �     V     q     x     |     �     �     �  .   �     �      �                                      
                                            	                      API Service Key API Service URL Add the API Service 'API Key'  Add the API Service URL Add the NLF Teams List header Add the NLF Teams List subheader Conference Display Name Division Error connecting to data services. Please review the admin settings or try again later. ID Invalid credentials. Please review the admin settings. NFL Data Settings Name Next Nickname Prev Search Team: Show _MENU_ Teams Showing _START_ to _END_ of _TOTAL_ Teams Teams List Header Teams List Subheader Project-Id-Version: nfl-data
PO-Revision-Date: 2021-08-15 15:07-0700
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: includes
X-Poedit-SearchPath-1: public
X-Poedit-SearchPath-2: admin
 Clave del servicio API URL del  servicio API Añade la clave del servicio API Añade la URL del  servicio API Añade el titulo de la lista de equipos de la NFL Añade el subtitulo de la lista de equipos de la NFL Conferencia Nombre a Mostrar División Error al conectarse a los servicios de datos. Por favor revisar la configuración o intente más tarde. Índice Credenciales invalidas. Por favor revise la configuración en el área de administración. Configuración de NFL Data Nombre Sig Alias Prev Buscar Equipo: Mostar _MENU_  Equipos Mostrando _START_ to _END_ of _TOTAL_  Equipos Titulo de la lista de equipos Subtitulo de la lista de equipos 