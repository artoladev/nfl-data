<?php
/**
 * The plugin bootstrap file
 *
 * NFL Data description
 *
 * @since             1.0.0
 * @package           NFL_DATA
 *
 * @wordpress-plugin
 * Plugin Name:       NFL Data
 * Plugin URI:
 * Description:       The plugin's main purpose is to show the List of NFL teams filtered by conference and division.
 * Version:           1.0.0
 * Author:            Adan Artola
 * Author URI:        https://www.linkedin.com/in/adanartola/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nfl-data
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Current plugin's version.
 */
define( 'NFL_DATA', '1.0.0' );

/**
 * Plugin activation code.
 */
function activate_nfl_data() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nfl-data-activator.php';
	NFL_Data_Activator::activate();
}

/**
 * Plugin deactivation code.
 */
function deactivate_nfl_data() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nfl-data-deactivator.php';
	NFL_Data_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_nfl_Data' );
register_deactivation_hook( __FILE__, 'deactivate_nfl_Data' );

/**
 * Core plugin class used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-nfl-data.php';

/**
 * NFL data execution
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_nfl_data() {

	$plugin = new NFL_Data();
	$plugin->run();

}
run_nfl_data();
