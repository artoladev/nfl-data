<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    NFL_Data
 * @subpackage NFL_Data/public
 */

/**
 * The public-facing functionality of the plugin shortcode.
 *
 * Defines then necessary shortcode views, assets hooks, and action filters
 * in the public-facing part of the NFL Data plugin.
 *
 * @package    NFL_Data
 * @subpackage NFL_Data/public
 * @author     Adan Artola <adanartola@gmail.com>
 */
class Nfl_Data_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $nfl_data    The ID of this plugin.
	 */
	private $nfl_data;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * NFL Teams object
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $api_service   Teams data controller
	 */
	private $teams;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param    string $nfl_data   The name of the plugin.
	 * @param    string $version    The version of this plugin.
	 */
	public function __construct( $nfl_data, $version ) {

		$this->nfl_data = $nfl_data;
		$this->version  = $version;
		$this->teams    = new NFL_Data_Teams( $nfl_data );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * Loading the css bundle for the public area.
		 * To see the source code definition for this bundle, review
		 * resources/src/scss.
		 */
		wp_enqueue_style( $this->nfl_data, plugin_dir_url( __DIR__ ) . 'assets/css/bundle.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * Registering the javascript bundle for the public area.
		 * To see the source code definition for this bundle, review
		 * resources/src/js.
		 */
		wp_register_script( $this->nfl_data, plugin_dir_url( __DIR__ ) . 'assets/js/bundle.js', array( 'jquery' ), $this->version, true );

		/**
		 * The $data variable groups all the plufin necessary information
		 * in the JS fields.
		 */
		$data = array(
			'teams' => $this->teams::get_table_labels(), // Teams list table labels.
		);

		/**
		 * Adds the NFL_DATA_JS object to the bundle script.
		 */
		wp_localize_script( $this->nfl_data, 'NFL_DATA_JS', $data );

		/**
		 * Enqueue Public JS bundle to the WordPress footer.
		 */
		wp_enqueue_script( $this->nfl_data, plugin_dir_url( __DIR__ ) . 'assets/js/bundle.js', array( 'jquery' ), $this->version, true );

	}

	/**
	 * Register NFL Team List shorcode in WordPress
	 *
	 * @since    1.0.0
	 */
	public function add_team_list_shortcode() {

		add_shortcode( 'nflteamlist', array( $this, 'team_list_shortcode' ) );

	}

	/**
	 * Team List front end views
	 *
	 * @since    1.0.0
	 * @return   string  Shortcode output.
	 */
	public function team_list_shortcode() {
		/**
		 * Stops the oputputs strings from the shortcode
		 * until the required.
		 */
		ob_start();

		/**
		 * Actual API call.
		 */
		$is_loaded = $this->teams->load_teams_list();

		/**
		 * Show error message if API is not loaded correctly.
		 */
		if ( ! $is_loaded ) {
			$message = $this->teams->get_status_message();
			include plugin_dir_path( __FILE__ ) . 'partials/plugin-nfl-data-error-view.php';
			return ob_get_clean();
		}

		/**
		 * Get the Teams necessary information from the loaded
		 * API data.
		 */
		$teams          = $this->teams->get_teams_list();
		$columns        = $this->teams->get_list_columns();
		$list_header    = $this->teams->get_teams_list_header();
		$list_subheader = $this->teams->get_teams_list_subheader();

		/**
		 * Adding filters to allow updates on the data before rendering.
		 */
		$teams   = apply_filters( 'nfl_data_teams_list', $teams, $columns );
		$columns = apply_filters( 'nfl_data_teams_list_columns', $columns );

		/**
		 * Printing team list actual view.
		 */
		include plugin_dir_path( __FILE__ ) . 'partials/plugin-nfl-data-teams-list-view.php';

		/**
		 * Returns all the ouputs of the plugin.
		 */
		return ob_get_clean();
	}

}
