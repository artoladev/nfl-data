<?php
/**
 * Team list View
 *
 * This file is used to define the  markup in the NFL team list view.
 *
 * @since      1.0.0
 *
 * @package    NFL_Data
 * @subpackage NFL_Data/public/partials
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="nfl-data-main">
	<?php if ( ! empty( $list_header ) || ! empty( $list_subheader ) ) : ?>
		<div class="headers">			
			<h2 class="header--main" ><?php echo esc_html( $list_header ); ?></h2>		
			<h3 class="header--sub" ><?php echo esc_html( $list_subheader ); ?></h3>			
		</div>
	<?php endif; ?>
	<div class="team_list-scroll">
		<table id="teams-list" class="nfl-teams-list">
			<thead>
				<tr>
					<?php foreach ( $columns  as $key => $column ) : ?>
						<th><?php echo esc_html( $column ); ?></th>
					<?php endforeach; ?>            
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $teams  as $team ) : ?>
						<tr>
							<?php foreach ( $team  as $column => $value ) : ?>
								<td><?php echo esc_html( $value ); ?></td>
							<?php endforeach; ?>
						</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
