<?php
/**
 * Team list View
 *
 * This file is used to define the  markup in the NFL teams list error view. *
 *
 * @since      1.0.0
 *
 * @package    NFL_Data
 * @subpackage NFL_Data/public/partials
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="nfl-data-main">
	<h2 class="header--alert" > <?php echo esc_html( $message ); ?> </h>
</div>
